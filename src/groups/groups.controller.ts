import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Query,
} from '@nestjs/common';
import { GroupsService } from './groups.service';
import { CreateGroupDto } from './dto/create-group.dto';
import { UpdateGroupDto } from './dto/update-group.dto';
import { AddUserDto } from '../interests/dto/add-user.dto';

@Controller('groups')
export class GroupsController {
  constructor(private readonly groupsService: GroupsService) {}

  @Post('user')
  async addUser(@Body() addUserDto: AddUserDto) {
    console.log('asdad');
    return await this.groupsService.addToGroup(addUserDto);
  }

  @Delete('user')
  async removeUser(
    @Query('groupId') groupId: string,
    @Query('userId') userId: string,
  ) {
    return await this.groupsService.removeFromGroup(+groupId, +userId);
  }

  @Post()
  create(@Body() createGroupDto: CreateGroupDto) {
    return this.groupsService.create(createGroupDto);
  }

  @Get()
  async findAll() {
    return await this.groupsService.findAll();
  }

  @Get(':id')
  async findOne(@Param('id') id: string) {
    return await this.groupsService.findOne(+id);
  }

  @Patch(':id')
  async update(
    @Param('id') id: string,
    @Body() updateGroupDto: UpdateGroupDto,
  ) {
    return await this.groupsService.update(+id, updateGroupDto);
  }

  @Delete(':id')
  async remove(@Param('id') id: string) {
    return await this.groupsService.remove(+id);
  }

  @Get('user/:id')
  async findByUser(@Param('id') id: string) {
    return await this.groupsService.findByUserId(+id);
  }
}
