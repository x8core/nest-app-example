import { Injectable } from '@nestjs/common';
import { UpdateGroupDto } from './dto/update-group.dto';
import { PrismaService } from '../prisma/prisma.service';
import { Prisma, Group } from '@prisma/client';
import { AddUserDto } from '../interests/dto/add-user.dto';

@Injectable()
export class GroupsService {
  constructor(private readonly prisma: PrismaService) {}

  create(data: Prisma.GroupCreateInput) {
    return this.prisma.group.create({ data });
  }

  async findOne(id: number) {
    return await this.prisma.group.findUnique({ where: { id } });
  }

  async findAll(): Promise<Group[]> {
    return await this.prisma.group.findMany({
      include: {
        user: true,
      },
    });
  }

  async update(id: number, updateGroupDto: UpdateGroupDto) {
    return await this.prisma.group.update({
      where: { id },
      data: updateGroupDto,
    });
  }

  async remove(id: number) {
    return await this.prisma.interest.delete({ where: { id } });
  }

  async findByUserId(userId: number) {
    return this.prisma.group.findMany({
      where: {
        user: {
          some: { id: userId },
        },
      },
    });
  }

  async addToGroup(addUserDto: AddUserDto) {
    return await this.prisma.group.update({
      where: { id: addUserDto.groupId },
      data: {
        user: {
          connect: {
            id: addUserDto.userId,
          },
        },
      },
      include: { user: {} },
    });
  }

  async removeFromGroup(groupId: number, userId: number) {
    return await this.prisma.group.update({
      where: { id: groupId },
      data: {
        user: {
          disconnect: {
            id: userId,
          },
        },
      },
      include: { user: {} },
    });
  }
}
