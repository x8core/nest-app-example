import { IsNotEmpty, IsNumber } from 'class-validator';

export class AddUserDto {
  @IsNotEmpty()
  @IsNumber()
  userId: number;

  @IsNotEmpty()
  @IsNumber()
  groupId: number;
}
