import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  NotFoundException,
} from '@nestjs/common';
import { InterestsService } from './interests.service';
import { CreateInterestDto } from './dto/create-interest.dto';
import { UpdateInterestDto } from './dto/update-interest.dto';

@Controller('interests')
export class InterestsController {
  constructor(private readonly interestsService: InterestsService) {}

  @Post()
  async create(@Body() createInterestDto: CreateInterestDto) {
    return await this.interestsService.create(createInterestDto);
  }

  @Get()
  async findAll() {
    return await this.interestsService.findAll();
  }

  @Get(':id')
  async findOne(@Param('id') id: string) {
    return await this.interestsService.findOne(+id);
  }

  @Get('user/:id')
  async findByUser(@Param('id') id: string) {
    return await this.interestsService.findByUserId(+id);
  }

  @Patch(':id')
  async update(
    @Param('id') id: string,
    @Body() updateInterestDto: UpdateInterestDto,
  ) {
    const interest = await this.interestsService.findOne(+id);
    if (!interest) {
      throw new NotFoundException('Interest not found');
    }
    return await this.interestsService.update(+id, updateInterestDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.interestsService.remove(+id);
  }
}
