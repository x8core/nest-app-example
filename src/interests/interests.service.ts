import { Injectable } from '@nestjs/common';
import { UpdateInterestDto } from './dto/update-interest.dto';
import { PrismaService } from '../prisma/prisma.service';
import { Interest } from '@prisma/client';
import { CreateInterestDto } from './dto/create-interest.dto';

@Injectable()
export class InterestsService {
  constructor(private readonly prisma: PrismaService) {}

  create(data: CreateInterestDto) {
    return this.prisma.interest.create({ data });
  }

  async findOne(id: number) {
    return await this.prisma.interest.findUnique({ where: { id } });
  }

  async findByUserId(userId: number) {
    return this.prisma.interest.findMany({
      where: {
        user: {
          is: {
            id: { equals: userId },
          },
        },
      },
    });
  }

  async findAll(): Promise<Interest[]> {
    return await this.prisma.interest.findMany();
  }

  async update(id: number, updateInterestDto: UpdateInterestDto) {
    return await this.prisma.interest.update({
      where: { id },
      data: updateInterestDto,
    });
  }

  async remove(id: number) {
    return await this.prisma.interest.delete({ where: { id } });
  }
}
