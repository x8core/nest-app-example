import { Module } from '@nestjs/common';
//import { AppController } from './app.controller';
//iport { AppService } from './app.service';
import { UsersModule } from './users/users.module';
import { GroupsModule } from './groups/groups.module';
import { InterestsModule } from './interests/interests.module';
import { ConfigModule } from '@nestjs/config';
import { APP_GUARD } from '@nestjs/core';
import { AuthGuard } from './auth/auth.guard';

@Module({
  imports: [ConfigModule.forRoot(), UsersModule, GroupsModule, InterestsModule],
  //controllers: [AppController],
  providers: [
    //AppService,
    {
      provide: APP_GUARD,
      useClass: AuthGuard,
    },
  ],
})
export class AppModule {}
